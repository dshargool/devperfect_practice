f = open("input.txt", "r")
contents = f.read().splitlines()
theScorecard = {}
emotions = {'!love': 2, '!like': 1, '!dislike': -1, '!hate': -2}
for line in contents:
    splitStr = str.split(line, " ")
    # print(line)
    command = splitStr[0]
    twitchName = splitStr[1]
    if twitchName in theScorecard:
        theScorecard[twitchName] = theScorecard[twitchName] + emotions[command]
    else:
        theScorecard[twitchName] = emotions[command]
print(next(iter(sorted(theScorecard, key=theScorecard.get, reverse=True))))
